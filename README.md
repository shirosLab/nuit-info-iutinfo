# nuit-info

## Project setup
pour utiliser le site vous aurez besoin de node installé sur votre PC

faites 
``` npm install```
pour installer les dépendances du projet 

puis 
``` npm run dev```
Pour lancer le serveur local

Ce site internet a été réalisé durant la nuit de l’informatique lors de l’édition 2023.
La Nuit de l’Info est une compétition nationale qui réunit étudiants, enseignants et entreprises pour travailler ensemble sur le développement d’une application web. 15h de codage, de réflexion pour concocter une application web et de nombreux défis à relever comme de la programmation verte, de l'accessibilité ou bien des easter eggs.

L’application que nous avons créée doit permettre aux utilisateurs d’y voir plus clair sur les options qui existent pour faire face au changement climatique et de réaliser qu’un futur positif est à notre portée.

Notre équipe est composé de 7 membres parmi lesquels :
- Florien ALLARD
- Quentin COUTURIER
- Médéric DEMAILLY
- Noah DESPAUX
- Phuong Thao NGUYEN
- Mendy PAUL
- Jehan PHILIPON

Voici le lien de notre site web : http://nuit-info.kilya.net/
